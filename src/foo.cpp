#include <iostream>

/**
 * Prints a hello world text
 */
void hello_world()
{
    std::cout << "Hello, os-matcher!" << std::endl;
}
