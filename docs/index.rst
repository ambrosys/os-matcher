==========================================
OS-Matcher - Open Source Routing Framework
==========================================

.. note::
   .. mdinclude:: ../README.md
      :start-line: 1
      :end-line: 3

This is an example documentation.

``$ OsMatcher --help``

.. program-output:: _binaries/OsMatcher --help

.. toctree::
   :hidden:
   :caption: DEVELOPMENT

   _api/library_root
